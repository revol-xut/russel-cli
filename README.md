Russel-CLI Command Line Tool
--------------------------------

Tool for interfacing with the Russel daemon.

Contact: <revol-xut@protonmail.com>

## Examples

 - Request from the internal api how many ipc connections the daemon is holding.
```bash
 $ python3 main.py api --data={\"command\":\"connection_count\"} --id=1 --user true
```
  - Request percentual cpu usage from host system
```bash
$  python3 main.py api --data={\"command\":\"cpu\"} --id=21 --user true
```
  - Request Workload of engine
```bash
 python3 main.py workload --user true
 ```

  - List routines that are saved by the engine
```bash
 $ python3 main.py ls --user true
```

  upload --path=./testrutine.rt --name="TEST" --user true

 Command: api
 
 * --data The data container that get forwarded to the ipc socket handler.
 * --id Id of the element you want to talk to in this case ipc socket handler has the 1.
 * --user says that the ipc socket where the tool is trying connection to run /run/user/1000/russel.sock
 

## Requirements

+ **Python>=3.7**

## Change Log

**v0.1b** (2020.x.x)
 + Added more uniform error messages and responses
 + Added Command Interpreter Class

**v0.1a** (2020.2.12)

+ Added Basic Sockets
+ Added ping command which checks if the daemon is responding
+ Connections are now closed securely

