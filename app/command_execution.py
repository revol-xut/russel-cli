import base64
import json
import os

import errors
import object_ids
from ipc_socket import IPCSocket


class CommandExecution:
    _socket: IPCSocket = None

    def __init__(self, user_space: bool = False):
        ipc_socket: str

        if user_space:
            ipc_socket = "/run/user/1000/russel.sock"
        else:
            ipc_socket = "/run/russel.sock"

        self._socket = IPCSocket.create(ipc_socket)
        self._socket.connect()

    def __del__(self):
        self._socket.close()

    def execute(self, command: str, arguments: dict):

        if command == "ping":
            self._socket.send_string(json.dumps({'command': 'ping'}, ensure_ascii=True))
            res = self._socket.receive()
            if res["data"] == "heart-beat-return":
                print("Daemon is responding.")
            else:
                print("Daemon is not responding.")
        elif command == "api":
            if "id" not in arguments:
                raise KeyError(errors.id_not_found)
            if "data" not in arguments:
                raise KeyError(errors.data_not_found)

            try:
                data = json.loads(arguments["data"])
            except json.JSONDecodeError as e:
                raise e

            dict_data: dict = {"command": "api", "id": arguments["id"], "data": data}

            self._socket.send_string(json.dumps(dict_data, ensure_ascii=True))

            response: dict = self._socket.receive()

            if response is None:
                print("unknown endpoint !")
                return

            print("********************** Russel API Response **********************")

            for key in response.keys():
                print(key, ":", response[key])

        elif command == "upload":

            if "path" not in arguments:
                raise KeyError(errors.path_not_found)
            if "name" not in arguments:
                raise KeyError(errors.name_not_found)

            path: str = arguments["path"]

            if os.path.exists(path):
                # Reads file contents and closes it again
                with open(path) as f:
                    program: str = f.read()
                    size: int = len(program)

                dict_data: dict = {"command": "api", "id": object_ids.routine_manager_id,
                                   "data": {"command": "save_and_add", "name": arguments["name"],
                                            "program": base64.encode(program),
                                            "size": size}}
                self._socket.send_string(json.dumps(dict_data, ensure_ascii=True))
                response: dict = self._socket.receive()

                if "error" in response:
                    if response["error"] == errors.file_already_exists:
                        print("A routine with this name already exists. You can overwrite it with --force")
                    else:
                        raise RuntimeError("Daemon responded: " + response["error"])

                if "success" in response:
                    print("Successfully uploaded routine !")

            else:
                print("given path is invalid")

        elif command == "stage":
            if "name" not in arguments:
                raise KeyError(errors.name_not_found)

            dict_data: dict = {"command": "api", "id": object_ids.execution_handler,
                               "data": {"command": "stage_routine", "name": arguments["name"]}}

            self._socket.send_string(json.dumps(dict_data, ensure_ascii=True))
            response: dict = self._socket.receive()

            if "error" in response:
                if response["error"] == errors.did_not_found_routine:
                    print("Given routine does not exists. Maybe upload it first.")
                else:
                    raise RuntimeError("Daemon responded: " + response["error"])

            if "id" in response:
                print("Russel loaded the routine the routine runtime id is: " + response["id"])

        elif command == "ls":
            dict_data: dict = {"command": "api", "id": 15,
                               "data": {"command": "list"}}
            self._socket.send_string(json.dumps(dict_data, ensure_ascii=True))
            response: dict = self._socket.receive()

            if "error" in response:
                if response["error"] == errors.file_already_exists:
                    print("A routine with this name already exists. You can overwrite it with --force")
                else:
                    raise RuntimeError("Daemon responded: " + response["error"])

            print(response)

        elif command == "workload":
            dict_data: dict = {"command": "workload"}

            self._socket.send_string(json.dumps(dict_data, ensure_ascii=True))
            response: dict = self._socket.receive()

            if "error" in response:
                if response["error"] == errors.file_already_exists:
                    print("A routine with this name already exists. You can overwrite it with --force")
                else:
                    raise RuntimeError("Daemon responded: " + response["error"])
            else:
                print(response)
