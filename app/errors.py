# This File contains error messages that are used by this tool and by the russel daemon


# Tool error messages
id_not_found: str = "id_not_found"
data_not_found: str = "data_not_found"
path_not_found: str = "path_not_found"
name_not_found: str = "name_not_found"

# Daemon error messages
file_already_exists: str = "file_already_exists"
did_not_found_routine: str = "did_not_found_routine"
