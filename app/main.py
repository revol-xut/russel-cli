import argparse

from command_execution import CommandExecution

parser_command = argparse.ArgumentParser(prog="Russel Command Line Tool")

subparsers = parser_command.add_subparsers(help='commands', title="commands", dest="command")
ls_parser = subparsers.add_parser('ls', help='lists all routines')
rm_parser = subparsers.add_parser('rm', help='removes given routine')
build_parser = subparsers.add_parser('build', help='builds routine from source')
start_parser = subparsers.add_parser('start', help='starts a given routine')
stop_parser = subparsers.add_parser('stop', help='stops given process')
ps_parser = subparsers.add_parser('ps', help='lists all running processes')
pause_parser = subparsers.add_parser('pause', help='pauses given process')
ping_parser = subparsers.add_parser('ping', help='Checks if the daemon is responding')
api_parser = subparsers.add_parser('api', help='request data from internal api')
upload_parser = subparsers.add_parser('upload', help='uploads given routine')
workload_parser = subparsers.add_parser('workload',
                                        help='returns information about the resource consumption of local deamon')

api_parser.add_argument("--id", help='api id', dest="id", type=int)
api_parser.add_argument("--data", help='request data', dest="data")
api_parser.add_argument("--user", help='uses user space socket', dest="user", type=bool)

# https://stackoverflow.com/questions/29986185/python-argparse-dict-arg
ls_parser.add_argument("--long", type=bool, help='prints extra information')
ls_parser.add_argument("--user", help='uses user space socket', dest='user', type=bool)

ping_parser.add_argument("--user", help='uses user space socket', action='store_true')

upload_parser.add_argument("--path", type=str, help="path too routine file")
upload_parser.add_argument("--name", type=str, help="name of the routine")
upload_parser.add_argument("--user", help='uses user space socket', dest='user', type=bool)

workload_parser.add_argument("--user", help='uses user space socket', dest='user', type=bool)

if __name__ == "__main__":
    args = parser_command.parse_args()

    command_execution: CommandExecution = CommandExecution(args.user)

    command_execution.execute(args.command, vars(args))
