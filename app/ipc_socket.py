import json
import socket
import struct


class IPCSocket:
    sock: socket.socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    path: str = ""

    @staticmethod
    def create(ipc_socket_path: str) -> 'IPCSocket':
        this_socket: IPCSocket = IPCSocket()
        this_socket.path = ipc_socket_path
        return this_socket

    def close(self) -> None:
        self.send_string(json.dumps({"command": "close"}))
        self.sock.close()

    def connect(self) -> None:
        try:
            self.sock.connect(self.path)
        except FileNotFoundError as e:
            raise FileNotFoundError("ipc socket at: " + self.path + "not found !")

    def send_byte(self, message: bytes) -> None:
        try:
            self.sock.sendall(self.__encode_uint32(len(message) + 4) + message)
        except:
            raise FileNotFoundError("IPC socket form daemon not found. Is the daemon running")

    def send_string(self, message: str) -> None:
        try:
            self.send_byte(message.encode())
        except socket.error as _:
            raise FileNotFoundError("IPC socket from daemon not found. Is the daemon running ?")

    def receive(self) -> json:

        data = bytearray()
        n: int = 4
        header_size: bool = False

        while len(data) < n:
            packet = self.sock.recv(n - len(data))
            if not packet:
                break
            else:
                if not header_size:
                    n: int = self.__decode_uint32(packet[0:4]) - 4
                    header_size = True
                else:
                    data.extend(packet)
        my_json = data.decode('ascii').replace("'", '"').replace('\x00', '')
        return json.loads(my_json)

    @staticmethod
    def __encode_uint32(num: int) -> bytes:
        return struct.pack("I", num)

    @staticmethod
    def __decode_uint32(num: bytes) -> int:
        return struct.unpack("I", num)[0]
